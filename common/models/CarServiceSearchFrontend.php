<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * CarServiceSearch represents the model behind the search form of `app\models\CarService`.
 */
class CarServiceSearchFrontend extends Service
{
    public $manufacturer;
    public $service;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'description', 'address', 'email', 'slug', 'manufacturer', 'service',], 'safe'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'name'         => 'Название',
            'manufacturer' => 'Производитель авто',
            'service'      => 'Услуга',
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($slug)
    {
        $categoryId = Categories::findOne(['slug' => $slug])->id;
        $serviceId =
            ArrayHelper::map(
                CarService::find()
                    ->where(['category_id' => $categoryId])
                    ->asArray()->all(),
                'id',
                'service_id'
            );
        $query =
            Service::find()
                ->where(
                    [
                        'service.id' => $serviceId,
                        'service.is_status' => true,
                    ]
                )
                ->joinWith('servicesImages');
        
        $dataProvider = new ActiveDataProvider(
            [
                'query'      => $query,
                'pagination' => [
                    'pageSize' => 5,
                ],
            ]
        );
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        return $dataProvider;
    }
}
