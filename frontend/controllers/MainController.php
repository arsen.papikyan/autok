<?php

namespace frontend\controllers;

use Yii;
use yii\base\Controller;
use common\models\Pages;
use yii\web\NotFoundHttpException;

class MainController extends Controller
{

    public function actionIndex(){
        return $this->render('index');
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPages()
    {
        $getRequest = Yii::$app->request->get();
        $model = [];

        if ($getRequest['slugPages']) {
            $model =
                Pages::find()
                    ->where(
                        [
                            'slug' => $getRequest['slugPages'],
                            'is_status' => 1
                        ])
                    ->asArray()
                    ->one();
        }

        if (!empty($model)) {
            return $this->render(
                'pages',
                [
                    'model' => $model
                ]
            );
        }

        throw new NotFoundHttpException();
    }
}