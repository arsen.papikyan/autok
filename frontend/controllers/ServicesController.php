<?php

namespace frontend\controllers;

use common\models\CarServiceSearchFrontend;
use common\models\Service;
use Yii;
use yii\base\Controller;

class ServicesController extends Controller
{
    public function actionIndex()
    {
        
        $searchModel = new CarServiceSearchFrontend();
        $dataProvider = $searchModel->search(Yii::$app->request->get('slugCategories'));
        
        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }
    
    public function actionInformation()
    {
        $model =
            Service::find()
                ->where(
                    [
                        'slug' => Yii::$app->request->get('slugServices'),
                    ]
                )
                ->joinWith('servicesImages')
                ->one();
        
        return $this->render(
            'information',
            [
                'model' => $model,
            ]
        );
    }
}