<?php

namespace frontend\widgets;

use frontend\models\PricesSearch;
use yii\base\Widget;

class Price extends Widget
{
    public $serviceId;
    
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $searchModel = new PricesSearch();
        $dataProvider = $searchModel->search($this->serviceId);
        
        return $this->render(
            'price',
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]
        );
    }
}