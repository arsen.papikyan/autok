<?php
/**
 * Created by PhpStorm.
 * User: Arsen
 * Date: 8/06/2018
 * Time: 4:46 PM
 */

namespace frontend\widgets;


use common\models\Categories;
use yii\base\Widget;

class GetCategories extends Widget
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        $model =
            Categories::find()
                ->where(['is_status' => true])
                ->asArray()
                ->all();

        return $this->render(
            'categories',
            [
                'model' => $model
            ]
        );
    }
}