<div class="header">
    <!-- Logotype -->
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-sm-3 col-md-3 col-lg-3">
                <div class="outer">
                    <a href="<?= Yii::$app->homeUrl ?>" class="logo-text">
                        <img src="<?= Yii::$app->homeUrl ?>img/logo.png" alt=""></a>
                </div>
            </div>
            <div class="col-xs-4 col-sm-offset-7 col-sm-2 col-md-offset-8 col-md-1 col-lg-offset-8 col-lg-1">
                <div class="header-info">
                    <!-- Personal Menu -->
                    <div class="header-personal">
                        <a href="#" class="header-gopersonal"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>