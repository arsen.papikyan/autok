<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 text-align-center">
            <div class="circle">
                <i class="fas fa-car"></i>
            </div>
            <p>Выбери свой автомобиль</p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 text-align-center">
            <div class="circle">
                <i class="fas fa-wrench"></i>
            </div>
            <p>Реши, что чинить</p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 text-align-center">
            <div class="circle">
                <i class="fas fa-money-bill-alt"></i>
            </div>
            <p>Сравни цены и выбери автосервис</p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 text-align-center">
            <div class="circle">
                <i class="fas fa-percent"></i>
            </div>
            <p>Получи качественный ремонт со скидкой</p>
        </div>
    </div>
</div>

<?= \frontend\widgets\GetCategories::widget()?>