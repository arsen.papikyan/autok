<?php

$this->title = $model['title'];

$this->registerMetaTag(['name' => 'keywords', 'content' => $model['keywords']]);
$this->registerMetaTag(['name' => 'description', 'content' => $model['description']]);
?>

<section>
    <div class="frontsearch-wrap">
        <div class="cont frontsearch">
            <h2><?= $model['title']; ?></h2>
            <div class="frontsearch-wrap">
                <?= $model['content']; ?>
            </div>
            <span class="frontsearch-line1"></span>
            <span class="frontsearch-line2"></span>
        </div>
    </div>
</section>