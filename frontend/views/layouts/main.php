<?php

/* @var $this \yii\web\View */

/* @var $content string */

use frontend\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?= $this->render('../partials/header') ?>

<?= $content ?>

<?= $this->render('../partials/footer') ?>


<?php $this->endBody() ?>
<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCQX8vLycDu5QHjR6qP4h0d8e9NQN41JZY&signed_in=true&libraries=places&callback=initMap'></script>
</body>
</html>
<?php $this->endPage() ?>
