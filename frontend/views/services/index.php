<?php

/* @var $this yii\web\View */

use yii\widgets\ListView;

$this->title = 'Список автосервисов';
?>
<!-- Main Content - start -->
<main>


    <div class="cont maincont">

        <h1><span>Авто-сервисы</span></h1>
        <span class="maincont-line1 maincont-line12"></span>
        <span class="maincont-line2 maincont-line22"></span>

        <!-- Catalog Items - start -->
        <div class="section-list">
            <?= ListView::widget(
                [
                    'summary'      => '<p class="section-count">Найдено {totalCount} автосервисов</p>',
                    'dataProvider' => $dataProvider,
                    'itemView'     => '_list',
                    'pager'        =>
                        [
                            'options' =>
                                [
                                    'class' => 'pager',
                                ],
                        ],
                ]
            ); ?>
        </div>
    </div>
</main>
<!-- Main Content - end -->