'use strict';

function initMap() {

    show(JSON.parse($('#lat_lng').text()));

    function show(position) {
        console.log(position);
        var latlng = new google.maps.LatLng(position.lat,position.lng);

        var map = new google.maps.Map(document.getElementById('post-map1'), {
            center: latlng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: '',
            draggable: true
        });

        google.maps.event.addListener(marker, 'dragend', function(a) {
            document.getElementById('contact-lat_lng').value = JSON.stringify(a.latLng);
        });
    }
}