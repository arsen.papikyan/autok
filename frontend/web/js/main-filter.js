$('#city').suggestions({
    token: '9197ceb4a0b71545a32b2dcd37774181a48d6551',
    type: 'ADDRESS',
    hint: false,
    count: 3,
    onSelect: function(suggestion) {
        $('#fias_main').val(suggestion.data.city_fias_id);
    },
    bounds: 'city-settlement'
});

$('#city').blur(function(){
    if($('#fias_main').val()==''){
        $('#city').val('');
    }
    if($('#city').val()==''){
        $('#fias_main').val('');
    }
});

$.ajax({
    url: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/detectAddressByIp',
    headers: {
        Accept: 'application/json',
        Authorization: 'Token 9197ceb4a0b71545a32b2dcd37774181a48d6551'
    },
    success: function(data){
        $('#city').val(data.location.data.city);
        $('#fias_main').val(data.location.data.city_fias_id);
        console.log(data);
    }
});