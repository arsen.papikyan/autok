$.ajax({
    url: ' https://geocode-maps.yandex.ru/1.x/?format=json&geocode='+address,
    success: function(data){
        var coordinates = data.response.GeoObjectCollection.featureMember["0"].GeoObject.Point.pos.split(" ");
        google.maps.event.addDomListener(window, 'load', initialize(coordinates[1], coordinates[0]));
    }
});

"use strict";
function initialize(lang, long) {
    var mapOptions = {
        zoom: 15,
        scrollwheel: false,
        center: new google.maps.LatLng(lang, long)
    };
    var map = new google.maps.Map(document.getElementById('post-map1'),
        mapOptions);
    var marker = new google.maps.Marker({
        position: map.getCenter(),
        icon: '/img/marker.png',
        map: map
    });
}
