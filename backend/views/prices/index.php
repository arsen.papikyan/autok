<?php

use backend\modules\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PricesControl */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Prices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prices-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      =>
                [
                    ['class' => 'yii\grid\SerialColumn'],
                    
                    'id',
                    'service_id',
                    'category_id',
                    'manufacturer_id',
                    'description',
                    'passenger',
                    'offroad',
                    
                    ['class' => 'yii\grid\ActionColumn'],
                ],
        ]
    ); ?>
</div>
