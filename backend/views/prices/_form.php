<?php

use common\models\Service;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Prices */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prices-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?php
                
                $servicesModel =
                    ArrayHelper::map(
                        Service::find()->all(),
                        'id',
                        'name'
                    );
                
                echo $form->field($model, 'service_id')->widget(
                    Select2::class,
                    [
                        'data'    => $servicesModel,
                        'options' =>
                            [
                                'placeholder' => 'Services',
                                'onChange'    =>
                                    Sprintf(
                                    '$.get("%s",{
                                             serviceId: $(this).val() }
                                             ).done(function(data){
                                              $("#prices-category_id").html( data );
                                                }
                                              );
                                              $.get("%s",{
                                             serviceId: $(this).val() }
                                             ).done(function(data){
                                              $("#prices-manufacturer_id").html( data );
                                                }
                                              );
                                       ',
                                    Url::toRoute(['/prices/get-categories']),
                                    Url::toRoute(['/prices/get-manufacturer'])
                                ),
                            ],
                    ]
                );
                ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'category_id')->widget(
                    Select2::class,
                    [
                        'data'    => [],
                        'options' =>
                            [
                                'placeholder' => 'Categories',
                            ],
                    ]
                );
                ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <?= $form->field($model, 'manufacturer_id')->widget(
            Select2::class,
            [
                'data'    => [],
                'options' =>
                    [
                        'placeholder' => 'Manufacturer',
                    ],
            ]
        );
        ?>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'passenger')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'offroad')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <?= $form->field($model, 'description')->textarea(['row' => 3]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>
</div>
