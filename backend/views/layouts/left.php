<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= Yii::$app->homeUrl ?>images/users/man.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity['username'] ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat">
                    <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    ['label' => 'Categories', 'icon' => '', 'url' => ['/categories/index'],],
                    ['label' => 'Menu', 'icon' => '', 'url' => ['/menu/index'],],
                    ['label' => 'Pages', 'icon' => '', 'url' => ['/pages/index'],],
                    ['label' => 'Services', 'icon' => '', 'url' => ['/service/index'],],
                    ['label' => 'Prices', 'icon' => '', 'url' => ['/prices/index'],],
                ],
            ]
        ) ?>
    </section>

</aside>
