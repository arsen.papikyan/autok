<?php

use yii\helpers\Html;
use backend\modules\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ServiceControl */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Services';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            'seo_keyword',
            'seo_description',
            'description:ntext',
            //'address',
            'email:email',
            //'phone',
            //'g_maps_address',
            //'slug',
            //'car_model_id',
            //'discount',
            //'is_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
