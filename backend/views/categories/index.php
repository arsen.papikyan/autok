<?php

use yii\helpers\Html;
use backend\modules\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategoriesControl */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' =>
            [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'title',
                'slug',
                'keywords',
                'description:ntext',
                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
