<?php

namespace backend\controllers;

use Imagine\Image\Box;
use Yii;
use common\models\Categories;
use backend\models\CategoriesControl;
use yii\filters\AccessControl;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CategoriesController implements the CRUD actions for Categories model.
 */
class CategoriesController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow'   => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Lists all Categories models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategoriesControl();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }
    
    /**
     * Displays a single Categories model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render(
            'view',
            [
                'model' => $this->findModel($id),
            ]
        );
    }
    
    /**
     * Creates a new Categories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $model = new Categories();
        
        if ($model->load(Yii::$app->request->post())) {
            $imgFile = UploadedFile::getInstance($model, "img_name");
            
            if (!empty($imgFile)) {
                $imgPath = Yii::getAlias("@frontend")."/web/images/categories/";
                $imgName = Yii::$app->security->generateRandomString().'.'
                           .$imgFile->extension;
                $imgFile->saveAs($imgPath.$imgName);
                
                /**
                 * compress image, and  change size image
                 */
                $path = $imgPath.$imgName;
                
                $image = Image::getImagine()->open($path);
                
                /* small (changes img size) */
                $width = $image->getSize()->getWidth() >= 400
                    ? 400
                    : $image->getSize()->getWidth();
                $height = $image->getSize()->getWidth() >= 300
                    ? 300
                    : $image->getSize()->getWidth();
                
                $image->thumbnail(new Box($width, $height))
                    ->save($path, ['jpeg_quality' => 50])
                    ->save($path, ['png_compression_level' => 7])
                ;
                
                $model->img_name = $imgName;
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        
        return $this->render(
            'create',
            [
                'model' => $model,
            ]
        );
    }
    
    /**
     * Updates an existing Categories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImgName = $model->img_name;
        if ($model->load(Yii::$app->request->post())) {
            $imgFile = UploadedFile::getInstance($model, "img_name");
            
            if (!empty($imgFile)) {
                $imgPath = Yii::getAlias("@frontend")."/web/images/categories/";
                $imgName = Yii::$app->security->generateRandomString().'.'
                           .$imgFile->extension;
                $imgFile->saveAs($imgPath.$imgName);
                
                /**
                 * compress image, and  change size image
                 */
                $path = $imgPath.$imgName;
                $image = Image::getImagine()->open($path);
                
                /* small (changes img size) */
                $width = $image->getSize()->getWidth() >= 400 ? 400
                    : $image->getSize()->getWidth();
                $height = $image->getSize()->getWidth() >= 300 ? 300
                    : $image->getSize()->getWidth();
                
                $path = $imgPath.$imgName;
                $image->thumbnail(new Box($width, $height))
                    ->save($path, ['jpeg_quality' => 50])
                    ->save($path, ['png_compression_level' => 7])
                ;
                
                $model->img_name = $imgName;
            }
            else {
                $model->img_name = $oldImgName;
            }
            
            if ($model->save()) {
                
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        
        return $this->render(
            'update',
            [
                'model' => $model,
            ]
        );
        
    }
    
    /**
     * @param $id
     *
     * @return bool
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUploadImages($id)
    {
        $model = $this->findModel($id);
        $imgFile = UploadedFile::getInstance($model, "img_name");
        
        if (!empty($imgFile)) {
            $imgPath = Yii::getAlias("@frontend")."/web/images/categories/";
            $imgName = Yii::$app->security->generateRandomString().'.'
                       .$imgFile->extension;
            $imgFile->saveAs($imgPath.$imgName);
            
            /**
             * compress image, and  change size image
             */
            $path = $imgPath.$imgName;
            $image = Image::getImagine()->open($path);
            
            $width = $image->getSize()->getWidth() >= 400
                ? 400 : $image->getSize()->getWidth();
            $height = $image->getSize()->getHeight() >= 300
                ? 300 : $image->getSize()->getWidth();
            
            $image->thumbnail(new Box($width, $height))
                ->save($path, ['jpeg_quality' => 70])
                ->save($path, ['png_compression_level' => 7])
            ;
            
            $model->img_name = $imgName;
            $model->save();
        }
        
        return true;
    }
    
    public function actionDeleteFile($id)
    {
        $model = $this->findModel($id);
        
        $imgPath = Yii::getAlias('@frontend').'/web/images/categories/';
        if (file_exists($imgPath.$model->img_name)) {
            unlink($imgPath.$model->img_name);
            $model->img_name = null;
            $model->save();
            
            return true;
        }
        
        return false;
    }
    
    /**
     * Deletes an existing Categories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $imgPath = Yii::getAlias('@frontend').'/web/images/categories/';
        
        if (file_exists($imgPath.$model->img_name)) {
            unlink($imgPath.$model->img_name);
        }
        
        $model->delete();
        
        return $this->redirect(['index']);
    }
    
    /**
     * Finds the Categories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Categories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException(
                'The requested page does not exist.'
            );
        }
    }
}
