<?php

namespace backend\controllers;

use backend\models\PricesControl;
use common\models\CarManufacturer;
use common\models\CarService;
use common\models\Categories;
use common\models\Prices;
use common\models\Service;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PricesController implements the CRUD actions for Prices model.
 */
class PricesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Lists all Prices models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PricesControl();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render(
            'index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }
    
    /**
     * Displays a single Prices model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render(
            'view',
            [
                'model' => $this->findModel($id),
            ]
        );
    }
    
    /**
     * Finds the Prices model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Prices the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Prices::findOne($id)) !== null) {
            return $model;
        }
        
        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    /**
     * Creates a new Prices model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Prices();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        
        return $this->render(
            'create',
            [
                'model' => $model,
            ]
        );
    }
    
    /**
     * Updates an existing Prices model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        
        return $this->render(
            'update',
            [
                'model' => $model,
            ]
        );
    }
    
    /**
     * Deletes an existing Prices model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        
        return $this->redirect(['index']);
    }
    
    public function actionGetCategories($serviceId)
    {
        $categoriesId =
            ArrayHelper::map(
                CarService::find()
                    ->where(['service_id' => $serviceId])
                    ->asArray()
                    ->all(),
                'id',
                'category_id'
            );
        $modelCategory = Categories::find()
            ->where(['id' => $categoriesId])
            ->asArray()
            ->all();
        
        if ($modelCategory !== null) {
            foreach ($modelCategory as $category) {
                echo "<option value='{$category['id']}'>{$category['title']}</option>";
            }
        }
    }
    
    public function actionGetManufacturer($serviceId)
    {
        $manufacturerId =
            json_decode(
                Service::findOne(['id' => $serviceId])->car_manufacturer_id,
                true
            );
        var_dump($manufacturerId);
        
        $model = CarManufacturer::find()
            ->where(['id' => $manufacturerId])
            ->asArray()
            ->all();
        
        var_dump($model);
        
        if ($model !== null) {
            foreach ($model as $manufacturer) {
                echo "<option value='{$manufacturer['id']}'>{$manufacturer['name']}</option>";
            }
        }
    }
}
