'use strict';

function initMap() {
    show(JSON.parse($('#service-g_maps_address').val()));

    function show(position) {
        console.log(position);
        let latLng = new google.maps.LatLng(position.lat, position.lng);

        let map = new google.maps.Map(document.getElementById('map'), {
            center: latLng,
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        let marker = new google.maps.Marker({
            position: latLng,
            map: map,
            title: '',
            draggable: true
        });

        google.maps.event.addListener(marker, 'dragend', function (a) {
            document.getElementById('service-g_maps_address').value = JSON.stringify(a.latLng);
        });
    }
}